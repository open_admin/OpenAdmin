<?php
declare (strict_types=1);

namespace app\admin\controller\system;

use think\Response;
use app\common\enum\SystemEnum;
use hg\apidoc\annotation as Apidoc;
use think\db\exception\DbException;
use app\common\model\system\UserLogModel;
use app\common\controller\AdminController;
use app\common\validate\system\UserLogValidate;

/**
 * @Apidoc\Title("账户日志")
 * @Apidoc\Group("adminSystem")
 * @Apidoc\Sort("600")
 */
class UserLog extends AdminController
{
    /**
     * 初始化方法
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->model = new UserLogModel();
    }

    /**
     * @Apidoc\Title("列表")
     * @Apidoc\Param(ref="indexParam")
     * @Apidoc\Returned(ref="indexReturn")
     * @Apidoc\Returned("data", type="array", desc="内容列表",
     *     @Apidoc\Returned(ref="app\common\model\system\UserLogModel")
     * )
     * @return Response
     * @throws DbException
     */
    public function index(): Response
    {
        [$page, $limit, $where, $order] = $this->buildSelect();
        $data = $this->model->where($where)->order($order)->paginate(['list_rows' => $limit, 'page' => $page]);
        return $this->result($data->isEmpty() ? [] : $data->toArray());
    }

    /**
     * @Apidoc\Title("详情")
     * @Apidoc\Param(ref="app\common\model\system\UserLogModel",field="id")
     * @Apidoc\Returned(ref="app\common\model\system\UserLogModel")
     */
    public function read(): Response
    {
        $this->validate($this->param, UserLogValidate::class, 'read');
        $data = $this->model->findOrEmpty($this->param['id']);
        if ($data->isEmpty()) {
            return $this->error(SystemEnum::DATA_NOT_EXIST);
        }
        return $this->result($data->toArray());
    }

    /**
     * @Apidoc\Title("删除")
     * @Apidoc\Method("POST")
     * @Apidoc\Param("id",type="array|int",desc="主键ID")
     * @return Response
     */
    public function del(): Response
    {
        $this->validate($this->param, UserLogValidate::class, 'del');
        $data = $this->model->destroy($this->param['id']);
        if ($data) {
            return $this->result([]);
        }
        return $this->error(SystemEnum::DELETE_DATA_ERROR);
    }
}
