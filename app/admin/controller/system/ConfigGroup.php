<?php
declare (strict_types=1);

namespace app\admin\controller\system;

use think\Response;
use app\common\enum\SystemEnum;
use hg\apidoc\annotation as Apidoc;
use think\db\exception\DbException;
use app\common\controller\AdminController;
use app\common\model\system\ConfigGroupModel;
use app\common\validate\system\ConfigGroupValidate;

/**
 * @Apidoc\Title("配置分组")
 * @Apidoc\Group("adminSystem")
 * @Apidoc\Sort("100")
 */
class ConfigGroup extends AdminController
{
    /**
     * 初始化方法
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->model = new ConfigGroupModel();
    }

    /**
     * @Apidoc\Title("列表")
     * @Apidoc\Param(ref="indexParam")
     * @Apidoc\Returned(ref="indexReturn")
     * @Apidoc\Returned("data", type="array", desc="内容列表",
     *     @Apidoc\Returned(ref="app\common\model\system\ConfigGroupModel")
     * )
     * @return Response
     * @throws DbException
     */
    public function index(): Response
    {
        [$page, $limit, $where, $order] = $this->buildSelect();
        $data = $this->model->where($where)->order($order)->paginate(['list_rows' => $limit, 'page' => $page]);
        return $this->result($data->isEmpty() ? [] : $data->toArray());
    }

    /**
     * @Apidoc\Title("添加")
     * @Apidoc\Method("POST")
     * @Apidoc\Param(ref="app\common\model\system\ConfigGroupModel",withoutField="id,create_time,update_time")
     * @Apidoc\Returned(ref="app\common\model\system\ConfigGroupModel")
     */
    public function add(): Response
    {
        $this->validate($this->param, ConfigGroupValidate::class, 'add');
        $data = $this->model;
        $data->save($this->param);
        return $this->result($data->toArray());
    }

    /**
     * @Apidoc\Title("详情")
     * @Apidoc\Param(ref="app\common\model\system\ConfigGroupModel",field="id")
     * @Apidoc\Returned(ref="app\common\model\system\ConfigGroupModel")
     */
    public function read(): Response
    {
        $this->validate($this->param, ConfigGroupValidate::class, 'read');
        $data = $this->model->findOrEmpty($this->param['id']);
        if ($data->isEmpty()) {
            return $this->error(SystemEnum::DATA_NOT_EXIST);
        }
        return $this->result($data->toArray());
    }

    /**
     * @Apidoc\Title("修改")
     * @Apidoc\Method("POST")
     * @Apidoc\Param(ref="app\common\model\system\ConfigGroupModel",withoutField="create_time,update_time")
     * @Apidoc\Returned(ref="app\common\model\system\ConfigGroupModel")
     * @return Response
     */
    public function edit(): Response
    {
        $this->validate($this->param, ConfigGroupValidate::class, 'edit');
        $data = $this->model->findOrEmpty($this->param['id']);
        if ($data->isEmpty()) {
            return $this->error(SystemEnum::EDIT_DATA_ERROR);
        } else {
            $data->save($this->param);
        }
        return $this->result($data->toArray());
    }

    /**
     * @Apidoc\Title("删除")
     * @Apidoc\Method("POST")
     * @Apidoc\Param("id",type="array|int",desc="主键ID")
     * @return Response
     */
    public function del(): Response
    {
        $this->validate($this->param, ConfigGroupValidate::class, 'del');
        $data = $this->model->destroy($this->param['id']);
        if ($data) {
            return $this->result([]);
        }
        return $this->error(SystemEnum::DELETE_DATA_ERROR);
    }
}
