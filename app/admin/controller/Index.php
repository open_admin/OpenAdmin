<?php
declare (strict_types=1);

namespace app\admin\controller;

use think\Response;
use app\common\enum\SystemEnum;
use app\common\controller\AdminController;

class Index extends AdminController
{
    public function index(): Response
    {
        $data['info'] = 'OpenAdmin';
        return $this->result($data);
    }

    /**
     * 全局路由访问错误控制器
     * @return Response
     */
    public function miss(): Response
    {
        return $this->error(SystemEnum::ROUTE_MISS);
    }

}
