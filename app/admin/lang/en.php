<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About 
// +----------------------------------------------------------------------
// | 修改时间:2022/8/7-13:24
// +----------------------------------------------------------------------
return [
    //系统类错误信息
    '200'   => 'success',
    '401'   => 'Token does not exist',
    '403'   => 'No operation permission',
    '404'   => 'Route does not exist',
    '500'   => 'fail',

    //自定义类错误枚举
    '10001' => 'Exception while editing data',
    '10002' => 'Delete data exception',
    '10003' => 'The password is incorrect',
    '10004' => 'The account does not exist or has been disabled',
    '10005' => 'Login credentials have expired. Please login again',
    '10006' => 'Token service unknown exception',
    '10007' => 'The account has been logged in on another device',
    '10008' => 'login timeout',
    '10009' => 'Data does not exist or has been deleted',
];