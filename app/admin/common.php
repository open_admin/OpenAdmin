<?php
// 这是系统自动生成的公共文件

/**
 * 根据角色ID查询对应赋权的菜单ID数组
 * 角色未禁用,对应菜单不为空
 * @param array $role_id 角色ID数组
 * @return array 返回菜单ID数组
 */
function get_menu_id(array $role_id): array
{
    $menu_ids = [];
    if (!empty($role_id)) {
        $role_where[] = ['id', 'in', $role_id];                                                    //账户对应的角色ID
        $role_where[] = ['is_disable', '=', 0];                                                    //角色未禁用
        $role_where[] = ['menu_ids', '<>', ''];                                                    //角色对应的菜单权限不为空
        $role_ids     = \app\common\model\system\RoleModel::where($role_where)->column('menu_ids');//通过条件获取角色包含的菜单ID
        if (!empty($role_ids)) {
            if (sizeof($role_ids) > 1) {
                foreach ($role_ids as $v) {
                    $role_array[] = unserialize($v);
                }
                $menu_ids = array_unique(array_reduce($role_array, 'array_merge', array()));
            } else {
                $menu_ids = unserialize($role_ids[0]);
            }
        }
    }
    return $menu_ids;
}

/**
 * 根据后台用户ID和用户角色ID查询可以访问的菜单URL数组
 * 主要提供给鉴权使用
 * @param string|int $user_id 后台用户ID
 * @param array $role_id 用户对应角色ID
 * @return array
 */
function get_menu_auth_url(string|int $user_id, array $role_id): array
{
    $menu_url = \app\common\cache\system\MenuCache::get('auth_url_' . $user_id);
    if (empty($menu_url)) {
        $menu_ids     = get_menu_id($role_id);
        $menu_where[] = ['id', 'in', $menu_ids];                                                      //根据角色的包含的菜单ID查询
        $menu_where[] = ['after_route', '<>', ''];                                                    //后台路由地址不为空
        $menu_where[] = ['is_disable', '=', 0];                                                       //菜单未禁用
        $menu_url     = \app\common\model\system\MenuModel::where($menu_where)->column('after_route');//通过条件查询该账户可访问的权限菜单
        \app\common\cache\system\MenuCache::set('auth_url_' . $user_id, $menu_url);
    }
    return $menu_url;
}

/**
 * 获取无需登录的菜单URL
 * 用于鉴权中间件
 * @return array
 */
function get_not_login_url(): array
{
    $data = \app\common\cache\system\MenuCache::get('not_login');
    if (empty($data)) {
        $data = \app\common\model\system\MenuModel::where('is_login', 0)->where('is_disable', 0)->column('after_route');
        \app\common\cache\system\MenuCache::set('not_login', $data, 0);
    }
    return $data;
}