<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About 
// +----------------------------------------------------------------------
// | 修改时间:2022-08-24 00:47:01    该文件由自动生成器生成
// +----------------------------------------------------------------------
use think\facade\Route;

Route::group(function() {
    Route::rule('system/configgroup/index', 'system.ConfigGroup/index', 'GET');//配置分组列表
    Route::rule('system/configgroup/add', 'system.ConfigGroup/add', 'POST');//配置分组添加
    Route::rule('system/configgroup/read', 'system.ConfigGroup/read', 'GET');//配置分组详情
    Route::rule('system/configgroup/edit', 'system.ConfigGroup/edit', 'POST');//配置分组编辑
    Route::rule('system/configgroup/del', 'system.ConfigGroup/del', 'POST');//配置分组删除
    Route::rule('system/config/index', 'system.Config/index', 'GET');//系统配置列表
    Route::rule('system/config/add', 'system.Config/add', 'POST');//系统配置添加
    Route::rule('system/config/read', 'system.Config/read', 'GET');//系统配置详情
    Route::rule('system/config/edit', 'system.Config/edit', 'POST');//系统配置编辑
    Route::rule('system/config/del', 'system.Config/del', 'POST');//系统配置删除
    Route::rule('system/menu/index', 'system.Menu/index', 'GET');//系统菜单列表
    Route::rule('system/menu/add', 'system.Menu/add', 'POST');//系统菜单添加
    Route::rule('system/menu/read', 'system.Menu/read', 'GET');//系统菜单详情
    Route::rule('system/menu/edit', 'system.Menu/edit', 'POST');//系统菜单编辑
    Route::rule('system/menu/del', 'system.Menu/del', 'POST');//系统菜单删除
    Route::rule('system/role/index', 'system.Role/index', 'GET');//角色管理列表
    Route::rule('system/role/add', 'system.Role/add', 'POST');//角色管理添加
    Route::rule('system/role/read', 'system.Role/read', 'GET');//角色管理详情
    Route::rule('system/role/edit', 'system.Role/edit', 'POST');//角色管理编辑
    Route::rule('system/role/del', 'system.Role/del', 'POST');//角色管理删除
    Route::rule('system/user/index', 'system.User/index', 'GET');//账户管理列表
    Route::rule('system/user/add', 'system.User/add', 'POST');//账户管理添加
    Route::rule('system/user/read', 'system.User/read', 'GET');//账户管理详情
    Route::rule('system/user/edit', 'system.User/edit', 'POST');//账户管理编辑
    Route::rule('system/user/del', 'system.User/del', 'POST');//账户管理删除
    Route::rule('system/userlog/index', 'system.UserLog/index', 'GET');//后台日志列表
    Route::rule('system/userlog/read', 'system.UserLog/read', 'GET');//后台日志详情
    Route::rule('system/userlog/del', 'system.UserLog/del', 'POST');//后台日志删除
    Route::rule('system/user/login', 'system.User/login', 'POST');//账户管理登录
    Route::rule('system/user/logout', 'system.User/logout', 'POST');//账户管理退出
})->middleware('check');
Route::miss('Index/index/miss');
