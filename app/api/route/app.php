<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About
// +----------------------------------------------------------------------
// | 修改时间:2022/6/10-16:05
// +----------------------------------------------------------------------
use think\facade\Route;

Route::group(function () {
    Route::get('', 'Index/index');
    Route::get('index', 'Index/index');
});

//全局路由未匹配
Route::miss(function () {
    $param['code']    = '201';
    $param['message'] = 'route miss';
    $param['data']    = '';
    return json($param);
});