<?php
declare (strict_types=1);

namespace app\api\controller;

use think\facade\Cache;

class Index
{
    public function index()
    {
        Cache::clear();//测试时候使用
        return '您好！这是一个[api]示例应用';
    }
}
