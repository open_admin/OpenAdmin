<?php
declare (strict_types=1);

namespace app;

use think\App;
use think\Validate;
use app\common\trait\ResultTrait;

/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * 引入接口统一返回快捷操作
     */
    use ResultTrait;

    /**
     * Request实例
     * @var \think\Request
     */
    protected \think\Request $request;

    /**
     * 应用实例
     * @var App
     */
    protected App $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected bool $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected array $middleware = [];

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize(): void
    {

    }

    /**
     * 验证数据
     * @access protected
     * @param array $data 数据
     * @param array|string $validate 验证器名或者验证规则数组
     * @param array|string $message 提示信息|验证场景
     * @param bool $batch 是否批量验证
     * @return array|string|true
     */
    protected function validate(array $data, array|string $validate, array|string $message = [], bool $batch = false): array|string|bool
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = str_contains($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
            if (is_string($message) && empty($scene)) {
                $v->scene($message);
            }
        }
        if (is_array($message)) $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

}
