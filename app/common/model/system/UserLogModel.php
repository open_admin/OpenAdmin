<?php
declare (strict_types=1);

namespace app\common\model\system;

use think\Model;
use think\model\concern\SoftDelete;

class UserLogModel extends Model
{
    //引入软删除trait
    use SoftDelete;

    // 表名
    protected $name = 'system_user_log';

    /**
     * 将请求参数数组转为可存储的string
     * @param $value
     * @return string
     */
    public function setRequestDataAttr($value): string
    {
        return empty($value) ? '' : serialize($value);
    }

    /**
     * 将请求参数序列化数据转为数组
     * @param $value
     * @return array
     */
    public function getRequestDataAttr($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }

    /**
     * 将返回参数数组转为可存储的string
     * @param $value
     * @return string
     */
    public function setResponseDataAttr($value): string
    {
        return empty($value) ? '' : serialize($value);
    }

    /**
     * 将返回参数序列化数据转为数组
     * @param $value
     * @return array
     */
    public function getResponseDataAttr($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }
}
