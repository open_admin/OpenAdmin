<?php
declare (strict_types=1);

namespace app\common\model\system;

use think\Model;
use think\model\concern\SoftDelete;
use app\common\cache\system\MenuCache;
use app\common\utils\CreateRouteUtils;

class MenuModel extends Model
{
    //引入软删除trait
    use SoftDelete;

    // 表名
    protected $name = 'system_menu';

    /**
     * 新增或更新数据后的操作
     * @return void
     */
    public static function onAfterWrite(): void
    {
        MenuCache::deleteAll();
        CreateRouteUtils::Admin();
    }

    /**
     * 删除数据后的操作
     * @return void
     */
    public static function onAfterDelete(): void
    {
        MenuCache::deleteAll();
        CreateRouteUtils::Admin();
    }
}
