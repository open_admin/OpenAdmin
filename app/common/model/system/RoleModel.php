<?php
declare (strict_types=1);

namespace app\common\model\system;

use think\Model;
use think\model\concern\SoftDelete;
use app\common\cache\system\MenuCache;

class RoleModel extends Model
{
    //引入软删除trait
    use SoftDelete;

    // 表名
    protected $name = 'system_role';

    /**
     * 当前模型更新数据后的操作
     * @return void
     */
    public static function onAfterUpdate(): void
    {
        MenuCache::deleteAll();//清除菜单标签下的所有缓存
    }

    /**
     * 当前模型删除数据后的操作
     * @return void
     */
    public static function onAfterDelete(): void
    {
        MenuCache::deleteAll();//清除菜单标签下的所有缓存
    }

    /**
     * 将菜单数组转为可存储的string
     * @param $value
     * @return string
     */
    public function setMenuIdsAttr($value): string
    {
        return empty($value) ? '' : serialize($value);
    }

    /**
     * 将菜单序列化数据转为数组
     * @param $value
     * @return array
     */
    public function getMenuIdsAttr($value): array
    {
        //根据菜单ID获取所有父级ID,并将父级ID从数组中移除,父级的选择状态由前端关联决定
        $menu_ids = [];
        if (!empty($value)) {
            $menu_id  = unserialize($value);
            $menu_pid = MenuModel::where('id', 'in', $menu_id)->column('pid');//获取角色对应的所有菜单父级ID
            foreach (array_unique($menu_pid) as $v) {
                $key = array_search($v, $menu_id);
                if ($key !== false) {
                    unset($menu_id[$key]);
                }
            }
            $menu_ids = array_values($menu_id);
        }
        return $menu_ids;
    }
}
