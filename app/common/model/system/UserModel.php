<?php
declare (strict_types=1);

namespace app\common\model\system;

use think\Model;
use think\model\concern\SoftDelete;
use app\common\cache\system\UserCache;
use app\common\cache\system\MenuCache;

class UserModel extends Model
{
    //引入软删除trait
    use SoftDelete;

    // 表名
    protected $name = 'system_user';

    //全局隐藏字段
    protected $hidden = ['password'];

    /**
     * 更新数据后的操作
     * @param $model //当前模型的对象实例
     * @return void
     */
    public static function onAfterUpdate($model): void
    {
        UserCache::delete($model->id);              //删除被更新用户的账户缓存
        MenuCache::delete($model->id);              //删除被更新用户的菜单缓存
        MenuCache::delete('auth_url_' . $model->id);//删除被更新用户的权限列表
    }

    /**
     * 删除数据后的操作
     * @param $model //当前模型的对象实例
     * @return void
     */
    public static function onAfterDelete($model): void
    {
        UserCache::delete($model->id);
    }

    /**
     * 将明文密码转为md5加密密码
     * @param $value
     * @return string
     */
    public function setPassWordAttr($value): string
    {
        return md5($value);
    }

    /**
     * 将菜单数组转为可存储的string
     * @param $value
     * @return string
     */
    public function setRoleIdsAttr($value): string
    {
        return empty($value) ? '' : serialize($value);
    }

    /**
     * 将菜单序列化数据转为数组
     * @param $value
     * @return array
     */
    public function getRoleIdsAttr($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }
}
