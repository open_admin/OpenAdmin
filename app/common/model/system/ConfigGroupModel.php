<?php
declare (strict_types=1);

namespace app\common\model\system;

use think\Model;

class ConfigGroupModel extends Model
{
    // 表名
    protected $name = 'system_config_group';

}
