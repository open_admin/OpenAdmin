<?php
declare (strict_types=1);

namespace app\common\model\system;

use think\Model;
use think\model\concern\SoftDelete;
use app\common\cache\system\ConfigCache;

class ConfigModel extends Model
{
    //引入软删除trait
    use SoftDelete;

    // 表名
    protected $name = 'system_config';

    /**
     * 当前模型更新数据后的操作
     * @return void
     */
    public static function onAfterUpdate(): void
    {
        ConfigCache::deleteAll();//清除标签下的所有配置缓存
    }

    /**
     * 当前模型删除数据后的操作
     * @return void
     */
    public static function onAfterDelete(): void
    {
        ConfigCache::deleteAll();//清除标签下的所有配置缓存
    }
}
