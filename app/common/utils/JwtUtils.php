<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About 
// +----------------------------------------------------------------------
// | 修改时间:2022/8/1-16:06
// +----------------------------------------------------------------------
namespace app\common\utils;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use think\facade\Request;
use app\common\enum\SystemEnum;
use Firebase\JWT\ExpiredException;

class JwtUtils
{
    /**
     * 生成JWT验证Token信息
     * @param array $user_info 成功登录后的用户信息
     * @param int $token_expires Token的有效期(小时为单位)
     * @param string $token_key Token的加密key
     * @return string 返回一个Token字符串
     */
    public static function createToken(array $user_info, int $token_expires, string $token_key): string
    {
        //组装Token签名参数
        $domain  = Request::domain();
        $time    = time();
        $payload = [
            'iss'  => $domain,//签发者
            'aud'  => $domain,//接收者
            'iat'  => $time,//签发时间
            'nbf'  => $time,//生效时间
            'exp'  => $time + $token_expires * 3600,//过期时间
            'data' => $user_info,//自定义信息
        ];
        return JWT::encode($payload, $token_key, 'HS256');
    }

    /**
     * 验证Token并返回验证结果和自定义参数
     * @param string $token 待验证的Token字符串
     * @param string $token_key Token的加密key
     * @return array|int 返回一个Token内的自定义对象
     */
    public static function verifyToken(string $token, string $token_key): array|int
    {
        try {
            $decode = JWT::decode($token, new Key($token_key, 'HS256'));
            $data   = (array)$decode->data;
        } catch (ExpiredException) {
            return SystemEnum::TOKEN_EXPIRED;//TOKEN过期返回状态码
        } catch (\Exception) {
            return SystemEnum::TOKEN_SERVICE_ERROR;//如果验证TOKEN其他异常
        }
        return $data;
    }
}