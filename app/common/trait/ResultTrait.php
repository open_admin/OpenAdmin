<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About 
// +----------------------------------------------------------------------
// | 修改时间:2022/8/6-17:29
// +----------------------------------------------------------------------
namespace app\common\trait;

use think\Response;
use app\common\enum\SystemEnum;

/**
 * 全局返回特性
 * @package app\common\trait
 */
trait ResultTrait
{
    /**
     * 操作失败统一返回快捷方法
     * @param int $code 返回状态码
     * @param string|null $message 返回状态-自定义消息
     * @param array $data 返回数据
     * @return Response
     */
    protected function error(int $code = SystemEnum::ERROR, string $message = null, array $data = []): Response
    {
        return $this->result($data, $code, $message);
    }

    /**
     * 操作成功统一返回快捷方法
     * @param array $data 返回数据
     * @param int $code 返回状态码
     * @param string|null $message 返回状态-自定义消息
     * @return Response
     */
    protected function result(array $data = [], int $code = SystemEnum::SUCCESS, string $message = null): Response
    {
        $result['code']    = $code;
        $result['message'] = empty($message) ? lang((string)$code) : $message;
        $result['data']    = $data;

        return json($result);
    }
}