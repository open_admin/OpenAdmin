<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About 
// +----------------------------------------------------------------------
// | 修改时间:2022/8/15-20:00
// +----------------------------------------------------------------------
namespace app\common\cache;

use think\facade\Cache;

class BaseCache
{
    /**
     * 缓存名称前缀
     * @var string
     */
    protected static string $cache_name = 'base';

    /**
     * 根据key的额外参数获取缓存信息
     * @param string|int $key 拼凑key的额外参数
     * @return mixed
     */
    public static function get(string|int $key): mixed
    {
        return Cache::get(static::key($key));
    }

    /**
     * 获取缓存的完整key
     * @param string|int $key 拼凑key的额外参数
     * @return string
     */
    public static function key(string|int $key): string
    {
        return static::$cache_name . '_' . $key;
    }

    /**
     * 添加缓存
     * @param string|int $key 拼凑key的额外参数
     * @param mixed $data 缓存的内容
     * @param int $expires 缓存过期时间
     * @return bool
     */
    public static function set(string|int $key, mixed $data, int $expires = 86400): bool
    {
        return Cache::tag(static::$cache_name)->set(static::key($key), $data, $expires);
    }

    /**
     * 缓存删除
     * @param string|int|array $key 拼凑key的额外参数
     * @return bool
     */
    public static function delete(string|int|array $key): bool
    {
        $data = false;
        if (is_array($key)) {
            foreach ($key as $v) {
                $data = Cache::delete(static::key($v));
            }
        } else {
            $data = Cache::delete(static::key($key));
        }
        return $data;
    }

    /**
     * 删除当前标签下的所有缓存-谨慎使用
     * @return bool
     */
    public static function deleteAll(): bool
    {
        return Cache::tag(static::$cache_name)->clear();
    }
}