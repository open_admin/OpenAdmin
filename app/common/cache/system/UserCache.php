<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About 
// +----------------------------------------------------------------------
// | 修改时间:2022/8/15-20:40
// +----------------------------------------------------------------------
namespace app\common\cache\system;

use app\common\cache\BaseCache;

class UserCache extends BaseCache
{
    /**
     * 缓存名称前缀-不设置则使用默认前缀
     * @var string
     */
    protected static string $cache_name = 'system_user';
}