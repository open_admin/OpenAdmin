<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About
// +----------------------------------------------------------------------
// | 修改时间:2022/6/24-13:11
// +----------------------------------------------------------------------
namespace app\common\controller;

use think\Model;
use app\BaseController;

class AdminController extends BaseController
{
    /**
     * 当前模型
     * @var Model
     */
    protected Model $model;

    /**
     * 过滤后的请求数据
     * @var array
     */
    protected array $param;

    /**
     * 用户登录后的TOKEN自定义信息
     * @var array
     */
    protected array $token_info = [];

    /**
     * 用户登录后的缓存用户信息
     * @var array
     */
    protected array $cache_user_info = [];

    /**
     * 禁止新增和修改时修改的字段
     * @var array
     */
    protected array $exclude_field = ['create_time', 'update_time', 'delete_time'];

    /**
     * 初始化方法
     * @return void
     */
    public function initialize(): void
    {
        $this->param = $this->request->except($this->exclude_field);
        if (!empty($this->request->token_info)) {
            $this->token_info      = $this->request->token_info;
            $this->cache_user_info = $this->request->cache_user_info;
        }

    }

    /**
     * 组装查询参数
     * @return array
     */
    protected function buildSelect(): array
    {
        $get   = $this->request->get();
        $page  = !empty($get['page']) && is_numeric($get['page']) ? $get['page'] : 1;
        $limit = !empty($get['limit']) && is_numeric($get['limit']) ? $get['limit'] : 20;
        $order = !empty($get['order']) && is_array($get['order']) ? $get['order'] : ['id' => 'desc'];

        $where = [];

        if (!empty($get['field']) && is_array($get['field'])) {
            //组装条件
            $field = $get['field'];

            foreach ($field as $v) {
                switch (strtolower($v[1])) {
                    case '=':
                        $where[] = [$v[0], '=', $v[2]];
                        break;
                    case 'like':
                        $where[] = [$v[0], 'LIKE', "%{$v[2]}%"];
                        break;
                    case '>=':
                        try {
                            if ($this->validate([$v[0] => $v[2]], [$v[0] => ['require', 'dateFormat:Y-m-d H:i:s']]))
                                $where[] = [$v[0], '>=', $v[2]];
                        } catch (\Exception $e) {
                            //异常后不处理
                        }
                        break;
                    case '<=':
                        try {
                            if ($this->validate([$v[0] => $v[2]], [$v[0] => ['require', 'dateFormat:Y-m-d H:i:s']]))
                                $where[] = [$v[0], '<=', $v[2]];
                        } catch (\Exception $e) {
                            //异常后不处理
                        }
                        break;
                    default:
                }
            }
        }

        return [$page, $limit, $where, $order];
    }

}