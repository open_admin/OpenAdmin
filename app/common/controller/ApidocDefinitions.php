<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About
// +----------------------------------------------------------------------
// | 修改时间:2022/6/29-12:38
// +----------------------------------------------------------------------
namespace app\common\controller;

use hg\apidoc\annotation as Apidoc;

class ApidocDefinitions
{
    /**
     * 列表请求参数
     * @Apidoc\Param("page", type="int", default="1", desc="分页第几页")
     * @Apidoc\Param("limit", type="int", default="20", desc="分页每页数量")
     * @Apidoc\Param("order", type="array", desc="排序传递键值数组,默认主键排序,例:['id'=>'desc','sort'=>'desc']")
     * @Apidoc\Param("field", type="array", desc="查询条件,关联数组 例:['id','=','250'],['time','>=','2022-01-01 23:59:59']")
     */
    public function indexParam(): void
    {
    }

    /**
     * 列表返回参数
     * @Apidoc\Returned("total", type="int", desc="总数量")
     * @Apidoc\Returned("per_page", type="int", desc="每页规定返回数量")
     * @Apidoc\Returned("current_page", type="int", desc="当前页数")
     * @Apidoc\Returned("last_page", type="int", desc="总页数")
     */
    public function indexReturn(): void
    {
    }
}