<?php
declare (strict_types=1);

namespace app\common\validate\system;

use think\Validate;
use app\common\model\system\ConfigModel;

class ConfigGroupValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'   => ['require', 'integer', 'number'],
        'name' => ['require', 'max:16'],
        'code' => ['require', 'max:32', 'unique:system_config_group'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'   => '主键ID必填',
        'id.integer'   => '主键ID必须是整数',
        'id.number'    => '主键ID必须是数字',
        'name.require' => '分组名称必填',
        'name.max'     => '分组名称不得超过16个汉字',
        'code.require' => '编码名称必填',
        'code.max'     => '编码不得超过32个字符',
        'code.unique'  => '编码已存在不可重复',
    ];

    // 验证场景
    protected $scene = [
        'add'  => ['name', 'code'],
        'edit' => ['id', 'name', 'code'],
        'read' => ['id'],
    ];

    //删除场景剔除规则
    protected function sceneDel()
    {
        return $this->only(['id'])
                    ->remove('id', ['integer', 'number'])
                    ->append('id', 'checkConfig');
    }

    //自定义验证规则:验证分组内是否存在下级配置
    protected function checkConfig($value, $rule, $data = [])
    {
        $info = ConfigModel::where('group_id', '=', $data['id'])->select();
        if ($info->isEmpty()) {
            return true;
        }
        return '分组内有配置数据,不可删除';
    }
}
