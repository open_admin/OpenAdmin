<?php
declare (strict_types=1);

namespace app\common\validate\system;

use think\Validate;

class MenuValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'          => ['require', 'integer', 'number'],
        'pid'         => ['integer', 'number'],
        'name'        => ['require', 'max:64'],
        'type'        => ['require', 'between:1,3'],
        'mode'        => ['require', 'between:1,3'],
        'icon'        => ['max:64'],
        'controller'  => ['max:64'],
        'after_route' => ['max:64'],
        'front_route' => ['max:64'],
        'methods'     => ['max:16'],
        'links'       => ['activeUrl'],
        'sort'        => ['integer', 'number', 'max:11'],
        'is_basics'   => ['require', 'in:0,1'],
        'is_login'    => ['require', 'in:0,1'],
        'is_disable'  => ['in:0,1'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require' => '主键ID必填',
        'id.integer' => '主键ID必须是整数',
        'id.number'  => '主键ID必须是数字',
    ];

    // 验证场景
    protected $scene = [
        'add'  => ['pid', 'name', 'type', 'mode', 'icon', 'controller', 'after_route', 'front_route', 'methods', 'links', 'sort', 'is_basics', 'is_login', 'is_disable'],
        'edit' => ['id', 'pid', 'name', 'type', 'mode', 'icon', 'controller', 'after_route', 'front_route', 'methods', 'links', 'sort', 'is_basics', 'is_login', 'is_disable'],
        'read' => ['id'],
    ];

    //删除场景剔除规则
    public function sceneDel()
    {
        return $this->only(['id'])->remove('id', ['integer', 'number']);
    }
}
