<?php
declare (strict_types=1);

namespace app\common\validate\system;

use think\Validate;

class ConfigValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'         => ['require', 'integer', 'number'],
        'group_id'   => ['require', 'integer', 'number'],
        'group_code' => ['require', 'max:32'],
        'key'        => ['require', 'max:255'],
        'value'      => ['require', 'max:255'],
        'describe'   => ['require', 'max:64'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require' => '主键ID必填',
        'id.integer' => '主键ID必须是整数',
        'id.number'  => '主键ID必须是数字',
    ];

    // 验证场景
    protected $scene = [
        'add'  => ['group_id', 'group_code', 'key', 'value', 'describe'],
        'edit' => ['id', 'group_id', 'group_code', 'key', 'value', 'describe'],
        'read' => ['id'],
    ];

    //删除场景剔除规则
    public function sceneDel()
    {
        return $this->only(['id'])->remove('id', ['integer', 'number']);
    }
}
