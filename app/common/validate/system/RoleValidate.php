<?php
declare (strict_types=1);

namespace app\common\validate\system;

use think\Validate;

class RoleValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'         => ['require', 'integer', 'number'],
        'menu_ids'   => ['array'],
        'name'       => ['require', 'max:64'],
        'describe'   => ['max:255'],
        'sort'       => ['integer', 'number', 'max:11'],
        'is_disable' => ['in:0,1'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require' => '主键ID必填',
        'id.integer' => '主键ID必须是整数',
        'id.number'  => '主键ID必须是数字',
    ];

    // 验证场景
    protected $scene = [
        'add'  => ['menu_ids', 'name', 'describe', 'sort', 'is_disable'],
        'edit' => ['id', 'menu_ids', 'name', 'describe', 'sort', 'is_disable'],
        'read' => ['id'],
    ];

    //删除场景剔除规则
    public function sceneDel()
    {
        return $this->only(['id'])->remove('id', ['integer', 'number']);
    }
}
