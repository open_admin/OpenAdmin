<?php
declare (strict_types=1);

namespace app\common\validate\system;

use think\Validate;

class UserValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'         => ['require', 'integer', 'number'],
        'role_ids'   => ['array'],
        'account'    => ['require', 'max:64'],
        'password'   => ['require', 'max:64'],
        'nickname'   => ['require', 'max:64'],
        'phone'      => ['mobile'],
        'email'      => ['email', 'max:64'],
        'avatar_id'  => ['integer', 'number'],
        'introduce'  => ['max:255'],
        'is_super'   => ['in:0,1'],
        'is_disable' => ['in:0,1'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require' => '主键ID必填',
        'id.integer' => '主键ID必须是整数',
        'id.number'  => '主键ID必须是数字',
    ];

    // 验证场景
    protected $scene = [
        'add'   => ['role_ids', 'account', 'password', 'nickname', 'phone', 'email', 'avatar_id', 'introduce', 'is_super', 'is_disable'],
        'edit'  => ['id', 'role_ids', 'account', 'password', 'nickname', 'phone', 'email', 'avatar_id', 'introduce', 'is_super', 'is_disable'],
        'read'  => ['id'],
        'login' => ['account', 'password'],
    ];

    //删除场景剔除规则
    public function sceneDel()
    {
        return $this->only(['id'])->remove('id', ['integer', 'number']);
    }
}
