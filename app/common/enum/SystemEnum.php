<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About 
// +----------------------------------------------------------------------
// | 修改时间:2022/8/7-12:52
// +----------------------------------------------------------------------
namespace app\common\enum;

class SystemEnum
{
    //系统类错误信息枚举
    const SUCCESS         = 200;//请求成功
    const TOKEN_NOT_EXIST = 401;//Token未设置
    const NO_PERMISSION   = 403;//没有操作权限
    const ROUTE_MISS      = 404;//访问的路由不存在
    const ERROR           = 500;//系统错误

    //自定义类错误枚举
    const EDIT_DATA_ERROR     = 10001;//编辑数据时异常
    const DELETE_DATA_ERROR   = 10002;//删除数据异常
    const INCORRECT_PASSWORD  = 10003;//密码不正确
    const ACCOUNT_NOT_EXIST   = 10004;//账户不存在或已被禁用
    const TOKEN_EXPIRED       = 10005;//Token已过期请重新登录
    const TOKEN_SERVICE_ERROR = 10006;//Token服务异常
    const REPEAT_LOGIN        = 10007;//账户已在其他设备登录
    const LOGIN_TIMEOUT       = 10008;//登录超时
    const DATA_NOT_EXIST      = 10009;//数据不存在或已被删除
}