<?php
$subdirectory = '\test';//代码生成时的子目录可为空
return [
    // 文档标题
    'title'            => env('apidoc.title', 'API接口文档'),
    // 文档描述
    'desc'             => env('apidoc.desc', 'Apidoc'),
    // 默认请求类型
    'default_method'   => env('apidoc.default_method', 'GET'),
    // 默认作者名称
    'default_author'   => env('apidoc.default_author', 'OpenAdmin'),
    // 允许跨域访问
    'allowCrossDomain' => false,
    // 设置可选版本
    'apps'             => [
        [
            'title'   => '后台接口',
            'path'    => 'app\admin\controller',
            'folder'  => 'admin',
            'groups'  => [
                ['title' => '控制台', 'name' => 'adminConsole'],
                ['title' => '会员管理', 'name' => 'adminMember'],
                ['title' => '文件管理', 'name' => 'adminFile'],
                ['title' => '系统管理', 'name' => 'adminSystem']
            ],
            'headers' => [
                ['name' => env('token.system_token_name', 'X-System-Token'), 'type' => 'string', 'require' => true, 'desc' => '访问鉴权密钥,请求头内携带该参数']
            ]
        ],
        [
            'title'   => '对外接口',
            'path'    => 'app\api\controller',
            'folder'  => 'api',
            'groups'  => [
                ['title' => '首页', 'name' => 'index']
            ],
            'headers' => [
                ['name' => env('token.api_token_name', 'X-Api-Token'), 'type' => 'string', 'require' => true, 'desc' => '访问鉴权密钥,请求头内携带该参数']
            ]
        ]],
    // 自动生成url规则
    'auto_url'         => [
        // 字母规则
        //        'letter_rule'                => "lcfirst",
        //        // 多级路由分隔符
        //        'multistage_route_separator' => ".",
        'custom' => function ($class, $method) {
            $controller_layer = think\facade\Config::get('route.controller_layer', "controller") . '\\';//获取系统的控制器名称
            $substr           = substr(strstr($class, $controller_layer), strlen($controller_layer));//截取掉无用的部分
            $pathArr          = str_replace('\\', '.', $substr);//如果是多级目录进行转换
            $menu_info        = \app\common\model\system\MenuModel::where('controller', '=', $pathArr . '/' . $method)->findOrEmpty();//通过实际控制器地址获取菜单数据
            if ($menu_info->isEmpty()) return '菜单数据不存在请查看控制器实际路径是否设置正确,当前控制器路径[' . $pathArr . '/' . $method . ']';
            return empty($menu_info['after_route']) ? '菜单未设置路由地址请尽快设置' : $menu_info['after_route'];
        }
    ],
    // 指定公共注释定义的文件地址
    'definitions'      => "app\common\controller\ApidocDefinitions",
    // 缓存配置
    'cache'            => [
        // 是否开启缓存
        'enable' => env('apidoc.cache_enable', 'false'),
    ],
    // 权限认证配置
    'auth'             => [
        // 是否启用密码验证
        'enable'     => env('apidoc.auth_enable', false),
        // 全局访问密码
        'password'   => env('apidoc.auth_password', 'openadmin'),
        // 密码加密盐
        'secret_key' => env('apidoc.auth_secret_key', 'openadmin'),
        // 有效期
        'expire'     => env('apidoc.auth_expire', 86400)
    ],
    // 统一的请求Header
    'headers'          => [],
    // 统一的请求参数Parameters
    'parameters'       => [],
    // 统一的请求响应体
    'responses'        => [
        ['name' => 'code', 'desc' => '业务代码', 'type' => 'int'],
        ['name' => 'message', 'desc' => '业务信息', 'type' => 'string'],
        ['name' => 'data', 'desc' => '业务数据', 'main' => true, 'type' => 'object'],
    ],
    // md文档
    'docs'             => [],
    // 代码生成器配置 注意：是一个二维数组
    'generator'        => [
        [
            // 标题
            'title'      => '创建CRUD',
            // 是否启用
            'enable'     => env('apidoc.generator_enable', false),
            // 执行中间件，具体请查看下方中间件介绍
            'middleware' => [
                \app\common\middleware\CreateCrudMiddleware::class
            ],
            // 生成器窗口的表单配置
            'form'       => [
                // 表单显示列数
                'colspan' => 4,
                // 表单项字段配置
                'items'   => [
                    [
                        // 表单项标题
                        'title' => '控制器名称',
                        // 字段名
                        'field' => 'controller_title',
                        // 输入类型，支持：input、select
                        'type'  => 'input',
                    ], [
                        // 表单项标题
                        'title' => '上级菜单ID',
                        // 字段名
                        'field' => 'menu_id',
                        // 输入类型，支持：input、select
                        'type'  => 'input',
                        'rules' => [
                            ['required' => true, 'message' => '请输入上级菜单ID'],
                            ['pattern' => '^[0-9]{1,20}$', 'message' => '只能是数字'],
                        ]
                    ],
                ]
            ],
            // 文件生成配置，注意：是一个二维数组
            'files'      => [
                [
                    // 生成文件的文件夹地址，或php文件地址
                    'path'      => 'app\${app[0].folder}\controller' . $subdirectory,
                    // 生成文件的命名空间
                    'namespace' => 'app\${app[0].folder}\controller' . $subdirectory,
                    // 模板文件地址
                    'template'  => 'extend/apidoc/controller.tpl',
                    // 名称
                    'name'      => 'controller',
                    // 验证规则
                    'rules'     => [
                        ['required' => true, 'message' => '请输入控制器文件名'],
                        ['pattern' => '^[A-Z]{1}([a-zA-Z0-9]|[._]){2,19}$', 'message' => '名称不正确'],
                    ]
                ],
                [
                    'name'     => 'validate',
                    'path'     => 'app\common\validate' . $subdirectory,
                    'template' => 'extend/apidoc/validate.tpl',
                ],
                [
                    'name'     => 'cache',
                    'path'     => 'app\common\cache' . $subdirectory,
                    'template' => 'extend/apidoc/cache.tpl',
                ],
            ],
            // 数据表配置
            'table'      => [
                // 可选的字段类型
                'field_types' => [
                    "int",
                    "tinyint",
                    "integer",
                    "float",
                    "decimal",
                    "char",
                    "varchar",
                    "blob",
                    "text",
                    "point",
                ],
                // 数据表配置，注意：是一个二维数组，可定义多个数据表
                'items'       => [
                    [
                        // 表标题
                        'title'          => '主表',
                        // 模型名验证规则
                        'model_rules'    => [
                            ['pattern' => '^[A-Z]{1}([a-zA-Z0-9]|[._]){2,19}$', 'message' => '模型文件名错误，请输入大写字母开头的字母+数字，长度2-19的组合']
                        ],
                        // 表名验证规则
                        'table_rules'    => [
                            ['pattern' => '^[a-z]{1}([a-z0-9]|[_]){2,19}$', 'message' => '表名错误，请输入小写字母开头的字母+数字+下划线，长度2-19的组合']
                        ],
                        // 显示的提示文本
                        'desc'           => '[请勿删除默认的4个必要字段]----[除控制器外的其他文件名请使用类型作为后缀名称如:TestCache,以免引用重名]----[请确认好上级的菜单ID否则将导致菜单自动添加失败]',
                        // 生成模型的命名空间
                        'namespace'      => 'app\common\model' . $subdirectory,
                        // 生成模型的文件夹地址
                        'path'           => 'app\common\model' . $subdirectory,
                        // 模板文件地址
                        'template'       => 'extend/apidoc/model.tpl',
                        // 自定义配置列
                        'columns'        => [
                            [
                                'title' => '验证',
                                'field' => 'check',
                                'type'  => 'select',
                                'width' => 180,
                                'props' => [
                                    'placeholder' => '请输入',
                                    'mode'        => 'multiple',
                                    'maxTagCount' => 1,
                                    'options'     => [
                                        ['label' => '必填', 'value' => 'require', 'message' => '缺少必要参数{$item.field}'],
                                        ['label' => '数字', 'value' => 'number', 'message' => '{$item.field}字段类型为数字'],
                                        ['label' => '整数', 'value' => 'integer', 'message' => '{$item.field}为整数'],
                                        ['label' => '布尔', 'value' => 'boolean', 'message' => '{$item.field}为布尔值'],
                                    ],
                                ],
                            ],
                            [
                                'title' => '添加',
                                'field' => 'add',
                                'type'  => 'checkbox',
                                'width' => 60
                            ],
                            [
                                'title' => '修改',
                                'field' => 'edit',
                                'type'  => 'checkbox',
                                'width' => 60
                            ]
                        ],
                        // 默认字段
                        'default_fields' => [
                            [
                                // 字段名
                                'field'       => 'id',
                                // 字段注释
                                'desc'        => '主键id',
                                // 字段类型
                                'type'        => 'int',
                                // 字段长度
                                'length'      => 11,
                                // 默认值
                                'default'     => '',
                                // 非Null
                                'not_null'    => true,
                                // 主键
                                'main_key'    => true,
                                // 自增
                                'incremental' => true,
                            ],
                            [
                                'field'       => 'create_time',
                                'desc'        => '创建时间',
                                'type'        => 'datetime',
                                'length'      => null,
                                'default'     => '',
                                'not_null'    => false,
                                'main_key'    => false,
                                'incremental' => false,
                            ],
                            [
                                'field'       => 'update_time',
                                'desc'        => '更新时间',
                                'type'        => 'datetime',
                                'length'      => null,
                                'default'     => '',
                                'not_null'    => false,
                                'main_key'    => false,
                                'incremental' => false,
                            ],
                            [
                                'field'       => 'delete_time',
                                'desc'        => '删除时间',
                                'type'        => 'datetime',
                                'length'      => null,
                                'default'     => '',
                                'not_null'    => false,
                                'main_key'    => false,
                                'incremental' => false,
                            ]
                        ],
                        // 添加一行字段时，默认的值
                        'default_values' => [
                            //这里就是对应每列字段名=>值
                            'type'    => 'varchar',
                            'length'  => 255,
                            'default' => '',
                            //...
                        ],
                    ],
                ]
            ]
        ],
    ]
];
