<?php

// +----------------------------------------------------------------------
// | 缓存设置
// +----------------------------------------------------------------------

return [
    // 默认缓存驱动
    'default' => env('cache.driver', 'file'),

    // 缓存连接方式配置
    'stores'  => [
        'file' => [
            // 驱动方式
            'type'       => 'File',
            // 缓存保存目录
            'path'       => '',
            // 缓存前缀
            'prefix'     => '',
            // 缓存有效期 0表示永久缓存
            'expire'     => env('cache.expire', 86400),
            // 缓存标签前缀
            'tag_prefix' => 'op:',
            // 序列化机制 例如 ['serialize', 'unserialize']
            'serialize'  => [],
        ],
        'redis' => [
            // 驱动方式
            'type'      => 'redis',
            // 缓存有效期 0表示永久缓存
            'expire'    => env('cache.expire', 86400),
            // 序列化机制 例如 ['serialize', 'unserialize']
            'serialize' => [],
            // 使用的redis数据库的库名序号
            'select'    => env('cache.select', 0),
            // 缓存前缀
            'prefix'    => env('cache.prefix', 'op:'),
            // 主机
            'host'      => env('cache.host', '127.0.0.1'),
            // 端口
            'port'      => env('cache.port', 6379),
            // 密码
            'password'  => env('cache.password', ''),
        ],
        'memcache' => [
            // 驱动方式
            'type'      => 'memcache',
            // 缓存有效期 0表示永久缓存
            'expire'    => env('cache.expire', 86400),
            // 序列化机制 例如 ['serialize', 'unserialize']
            'serialize' => [],
            // 缓存前缀
            'prefix'    => env('cache.prefix', 'op:'),
            // 主机
            'host'      => env('cache.host', '127.0.0.1'),
            // 端口
            'port'      => env('cache.port', 11211),
            // 密码
            'password'  => env('cache.password', ''),
        ],
        // 更多的缓存连接
    ],
];
