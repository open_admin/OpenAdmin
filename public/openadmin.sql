SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for op_system_config
-- ----------------------------
DROP TABLE IF EXISTS `op_system_config`;
CREATE TABLE `op_system_config`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键自增ID',
  `group_id` int(11) NOT NULL COMMENT '配置组ID',
  `group_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置组标识码',
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置键',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置值',
  `describe` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置备注',
  `create_time` datetime(0) NOT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `group_code`(`group_code`) USING BTREE,
  INDEX `key`(`key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_system_config
-- ----------------------------
INSERT INTO `op_system_config` VALUES (1, 1, 'site', 'site_name', 'OpenAdmin后台管理系统', '网站名称', '2022-07-12 18:22:19', '2022-07-12 18:22:19', NULL);
INSERT INTO `op_system_config` VALUES (2, 1, 'site', 'site_logo', '/static/logo.svg', '网站LOGO', '2022-07-13 15:30:26', '2022-08-20 23:30:57', NULL);
INSERT INTO `op_system_config` VALUES (3, 1, 'site', 'site_copyright', '©2022 OpenAdmin', '版权信息', '2022-07-13 15:33:44', '2022-07-13 15:33:44', NULL);
INSERT INTO `op_system_config` VALUES (4, 1, 'site', 'site_log', 'true', '后台日志开关', '2022-07-13 15:43:10', '2022-08-08 17:37:46', NULL);
INSERT INTO `op_system_config` VALUES (5, 2, 'token', 'token_admin_key', 'ZNtHIJilQFVvfPB6', '管理员后台token密钥', '2022-08-03 19:19:53', '2022-08-03 19:19:53', NULL);
INSERT INTO `op_system_config` VALUES (6, 2, 'token', 'token_admin_expires', '12', '管理员后台token有效期(小时为单位)', '2022-08-03 19:21:38', '2022-08-03 19:21:38', NULL);
INSERT INTO `op_system_config` VALUES (7, 1, 'site', 'site_repeat_login', 'true', '管理后台单点登录开关,[false]关闭后支持多端同时登录', '2022-08-08 16:49:54', '2022-08-18 17:49:00', NULL);

-- ----------------------------
-- Table structure for op_system_config_group
-- ----------------------------
DROP TABLE IF EXISTS `op_system_config_group`;
CREATE TABLE `op_system_config_group`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键自增ID',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置组名称',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置组标识码',
  `create_time` datetime(0) NOT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置分组数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_system_config_group
-- ----------------------------
INSERT INTO `op_system_config_group` VALUES (1, '网站配置', 'site', '2022-07-12 17:38:33', '2022-07-12 17:38:33');
INSERT INTO `op_system_config_group` VALUES (2, '系统token配置', 'token', '2022-08-03 19:18:22', '2022-08-03 19:18:22');

-- ----------------------------
-- Table structure for op_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `op_system_menu`;
CREATE TABLE `op_system_menu`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键自增ID',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '菜单父级ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `type` tinyint(1) NOT NULL COMMENT '菜单类型[1菜单2按钮3虚拟菜单]',
  `mode` tinyint(1) NOT NULL COMMENT '菜单模式[1组件2内链3外链]',
  `icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `controller` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '控制器真实地址',
  `after_route` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后台路由地址',
  `front_route` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '前台路由地址',
  `methods` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法[get,post...]',
  `links` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '外链或内链的地址',
  `sort` int(11) NULL DEFAULT 100 COMMENT '排序',
  `is_basics` tinyint(1) NOT NULL DEFAULT 0 COMMENT '系统基础菜单[1是0否]',
  `is_login` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否需要登录[1是0否]',
  `is_disable` tinyint(1) NOT NULL DEFAULT 0 COMMENT '禁用[1是0否]',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `after_route`(`after_route`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_system_menu
-- ----------------------------
INSERT INTO `op_system_menu` VALUES (1, 0, '系统管理', 1, 1, 'IconSettings', NULL, NULL, 'system', NULL, NULL, 100, 0, 1, 0, '2022-07-13 15:50:41', '2022-08-12 22:32:01', NULL);
INSERT INTO `op_system_menu` VALUES (2, 1, '配置管理', 1, 1, 'IconTool', NULL, NULL, 'config', NULL, NULL, 100, 0, 1, 0, '2022-07-13 15:53:19', '2022-08-12 22:32:23', NULL);
INSERT INTO `op_system_menu` VALUES (3, 1, '菜单管理', 1, 1, 'IconAlignLeft', NULL, NULL, 'menu', NULL, NULL, 100, 0, 1, 0, '2022-07-13 15:58:18', '2022-08-12 22:32:31', NULL);
INSERT INTO `op_system_menu` VALUES (4, 1, '角色管理', 1, 1, 'IconSafe', NULL, NULL, 'role', NULL, NULL, 100, 0, 1, 0, '2022-07-13 15:59:08', '2022-08-12 22:32:42', NULL);
INSERT INTO `op_system_menu` VALUES (5, 1, '账户管理', 1, 1, 'IconUser', NULL, NULL, 'user', NULL, NULL, 100, 0, 1, 0, '2022-07-13 16:00:25', '2022-08-12 22:32:51', NULL);
INSERT INTO `op_system_menu` VALUES (6, 1, '后台日志', 1, 1, 'IconFile', NULL, NULL, 'userlog', NULL, NULL, 100, 0, 1, 0, '2022-07-13 16:00:59', '2022-08-12 22:33:05', NULL);
INSERT INTO `op_system_menu` VALUES (7, 2, '配置分组列表', 2, 1, NULL, 'system.ConfigGroup/index', '/admin/system/configgroup/index', 'system.group.list', 'get', NULL, 100, 0, 1, 0, '2022-07-13 16:04:54', '2022-08-13 15:46:38', NULL);
INSERT INTO `op_system_menu` VALUES (8, 2, '配置分组添加', 2, 1, NULL, 'system.ConfigGroup/add', '/admin/system/configgroup/add', 'system.group.add', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:06:40', '2022-08-13 15:26:25', NULL);
INSERT INTO `op_system_menu` VALUES (9, 2, '配置分组详情', 2, 1, NULL, 'system.ConfigGroup/read', '/admin/system/configgroup/read', 'system.group.read', 'get', NULL, 100, 0, 1, 0, '2022-07-13 16:08:56', '2022-08-13 15:26:35', NULL);
INSERT INTO `op_system_menu` VALUES (10, 2, '配置分组编辑', 2, 1, NULL, 'system.ConfigGroup/edit', '/admin/system/configgroup/edit', 'system.group.edit', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:10:04', '2022-08-13 15:26:48', NULL);
INSERT INTO `op_system_menu` VALUES (11, 2, '配置分组删除', 2, 1, NULL, 'system.ConfigGroup/del', '/admin/system/configgroup/del', 'system.group.dele', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:10:50', '2022-08-13 15:27:08', NULL);
INSERT INTO `op_system_menu` VALUES (12, 2, '系统配置列表', 2, 1, NULL, 'system.Config/index', '/admin/system/config/index', 'system.config.list', 'get', NULL, 100, 0, 1, 0, '2022-07-13 16:11:43', '2022-08-13 15:46:32', NULL);
INSERT INTO `op_system_menu` VALUES (13, 2, '系统配置添加', 2, 1, NULL, 'system.Config/add', '/admin/system/config/add', 'system.config.add', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:17:25', '2022-08-13 15:27:41', NULL);
INSERT INTO `op_system_menu` VALUES (14, 2, '系统配置详情', 2, 1, NULL, 'system.Config/read', '/admin/system/config/read', 'system.config.read', 'get', NULL, 100, 0, 1, 0, '2022-07-13 16:17:25', '2022-08-13 15:28:04', NULL);
INSERT INTO `op_system_menu` VALUES (15, 2, '系统配置编辑', 2, 1, NULL, 'system.Config/edit', '/admin/system/config/edit', 'system.config.edit', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:17:25', '2022-08-13 15:28:16', NULL);
INSERT INTO `op_system_menu` VALUES (16, 2, '系统配置删除', 2, 1, NULL, 'system.Config/del', '/admin/system/config/del', 'system.config.dele', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:17:25', '2022-08-13 15:28:29', NULL);
INSERT INTO `op_system_menu` VALUES (17, 3, '系统菜单列表', 2, 1, NULL, 'system.Menu/index', '/admin/system/menu/index', 'system.menu.list', 'get', NULL, 100, 1, 1, 0, '2022-07-13 16:20:53', '2022-08-18 16:14:25', NULL);
INSERT INTO `op_system_menu` VALUES (18, 3, '系统菜单添加', 2, 1, NULL, 'system.Menu/add', '/admin/system/menu/add', 'system.menu.add', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:20:53', '2022-08-13 15:30:08', NULL);
INSERT INTO `op_system_menu` VALUES (19, 3, '系统菜单详情', 2, 1, NULL, 'system.Menu/read', '/admin/system/menu/read', 'system.menu.read', 'get', NULL, 100, 0, 1, 0, '2022-07-13 16:20:53', '2022-08-13 15:30:21', NULL);
INSERT INTO `op_system_menu` VALUES (20, 3, '系统菜单编辑', 2, 1, NULL, 'system.Menu/edit', '/admin/system/menu/edit', 'system.menu.edit', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:20:53', '2022-08-13 15:30:36', NULL);
INSERT INTO `op_system_menu` VALUES (21, 3, '系统菜单删除', 2, 1, NULL, 'system.Menu/del', '/admin/system/menu/del', 'system.menu.dele', 'post', NULL, 100, 0, 1, 0, '2022-07-13 16:20:53', '2022-08-13 15:30:48', NULL);
INSERT INTO `op_system_menu` VALUES (22, 4, '角色管理列表', 2, 1, NULL, 'system.Role/index', '/admin/system/role/index', 'system.role.list', 'get', NULL, 100, 0, 1, 0, '2022-07-13 17:09:41', '2022-08-13 15:31:11', NULL);
INSERT INTO `op_system_menu` VALUES (23, 4, '角色管理添加', 2, 1, NULL, 'system.Role/add', '/admin/system/role/add', 'system.role.add', 'post', NULL, 100, 0, 1, 0, '2022-07-13 17:09:41', '2022-08-13 15:31:20', NULL);
INSERT INTO `op_system_menu` VALUES (24, 4, '角色管理详情', 2, 1, NULL, 'system.Role/read', '/admin/system/role/read', 'system.role.read', 'get', NULL, 100, 0, 1, 0, '2022-07-13 17:09:41', '2022-08-13 15:31:33', NULL);
INSERT INTO `op_system_menu` VALUES (25, 4, '角色管理编辑', 2, 1, NULL, 'system.Role/edit', '/admin/system/role/edit', 'system.role.edit', 'post', NULL, 100, 0, 1, 0, '2022-07-13 17:09:41', '2022-08-13 15:31:44', NULL);
INSERT INTO `op_system_menu` VALUES (26, 4, '角色管理删除', 2, 1, NULL, 'system.Role/del', '/admin/system/role/del', 'system.role.dele', 'post', NULL, 100, 0, 1, 0, '2022-07-13 17:09:41', '2022-08-13 15:31:56', NULL);
INSERT INTO `op_system_menu` VALUES (27, 5, '账户管理列表', 2, 1, NULL, 'system.User/index', '/admin/system/user/index', 'system.user.list', 'get', NULL, 100, 0, 1, 0, '2022-07-13 17:11:53', '2022-08-13 15:34:05', NULL);
INSERT INTO `op_system_menu` VALUES (28, 5, '账户管理添加', 2, 1, NULL, 'system.User/add', '/admin/system/user/add', 'system.user.add', 'post', NULL, 100, 0, 1, 0, '2022-07-13 17:11:53', '2022-08-13 15:34:16', NULL);
INSERT INTO `op_system_menu` VALUES (29, 5, '账户管理详情', 2, 1, NULL, 'system.User/read', '/admin/system/user/read', 'system.user.read', 'get', NULL, 100, 0, 1, 0, '2022-07-13 17:11:53', '2022-08-13 15:34:26', NULL);
INSERT INTO `op_system_menu` VALUES (30, 5, '账户管理编辑', 2, 1, NULL, 'system.User/edit', '/admin/system/user/edit', 'system.user.edit', 'post', NULL, 100, 0, 1, 0, '2022-07-13 17:11:53', '2022-08-13 15:34:39', NULL);
INSERT INTO `op_system_menu` VALUES (31, 5, '账户管理删除', 2, 1, NULL, 'system.User/del', '/admin/system/user/del', 'system.user.dele', 'post', NULL, 100, 0, 1, 0, '2022-07-13 17:11:53', '2022-08-13 15:34:52', NULL);
INSERT INTO `op_system_menu` VALUES (32, 6, '后台日志列表', 2, 1, NULL, 'system.UserLog/index', '/admin/system/userlog/index', 'system.user_log.list', 'get', NULL, 100, 0, 1, 0, '2022-07-13 17:14:38', '2022-08-13 15:37:17', NULL);
INSERT INTO `op_system_menu` VALUES (33, 6, '后台日志详情', 2, 1, NULL, 'system.UserLog/read', '/admin/system/userlog/read', 'system.user_log.read', 'get', NULL, 100, 0, 1, 0, '2022-07-13 17:14:38', '2022-08-13 15:37:29', NULL);
INSERT INTO `op_system_menu` VALUES (34, 6, '后台日志删除', 2, 1, NULL, 'system.UserLog/del', '/admin/system/userlog/del', 'system.user_log.dele', 'post', NULL, 100, 0, 1, 0, '2022-07-13 17:14:38', '2022-08-13 15:37:41', NULL);
INSERT INTO `op_system_menu` VALUES (35, 5, '账户管理登录', 3, 1, NULL, 'system.User/login', '/admin/system/user/login', NULL, 'post', NULL, 100, 1, 0, 0, '2022-08-07 21:24:43', '2022-08-13 15:36:47', NULL);
INSERT INTO `op_system_menu` VALUES (36, 5, '账户管理退出', 3, 1, NULL, 'system.User/logout', '/admin/system/user/logout', NULL, 'post', NULL, 100, 1, 1, 0, '2022-08-07 21:26:04', '2022-08-13 15:36:57', NULL);

-- ----------------------------
-- Table structure for op_system_role
-- ----------------------------
DROP TABLE IF EXISTS `op_system_role`;
CREATE TABLE `op_system_role`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键自增ID',
  `menu_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '菜单ID[数组]',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色备注',
  `sort` int(11) NULL DEFAULT 100 COMMENT '排序',
  `is_disable` tinyint(1) NOT NULL DEFAULT 0 COMMENT '禁用[1是0否]',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色权限信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_system_role
-- ----------------------------

-- ----------------------------
-- Table structure for op_system_user
-- ----------------------------
DROP TABLE IF EXISTS `op_system_user`;
CREATE TABLE `op_system_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键自增ID',
  `role_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '角色ID[数组]',
  `account` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '管理员账号',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '管理员密码',
  `nickname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '管理员昵称',
  `phone` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员手机',
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员邮箱',
  `avatar_id` int(11) NULL DEFAULT NULL COMMENT '头像对应的文件ID',
  `introduce` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '管理员简介',
  `login_number` int(11) NULL DEFAULT 0 COMMENT '管理员登录次数',
  `login_last_ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员最后登录IP',
  `login_last_ip_region` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员最后登录IP地址',
  `login_last_time` datetime(0) NULL DEFAULT NULL COMMENT '管理员最后登录时间',
  `is_super` tinyint(1) NOT NULL DEFAULT 0 COMMENT '超级管理员[1是0否]',
  `is_disable` tinyint(1) NOT NULL DEFAULT 0 COMMENT '禁用[1是0否]',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `account`(`account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统管理员用户数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_system_user
-- ----------------------------
INSERT INTO `op_system_user` VALUES (1, '', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '系统管理员', '13333333333', 'admin@openadmin.com', NULL, '系统管理员', 22, '192.168.1.66', '内网IP内网IP', '2022-08-20 23:33:11', 1, 0, '2022-08-09 00:20:59', '2022-08-20 23:33:11', NULL);

-- ----------------------------
-- Table structure for op_system_user_log
-- ----------------------------
DROP TABLE IF EXISTS `op_system_user_log`;
CREATE TABLE `op_system_user_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键自增ID',
  `system_user_id` int(11) NOT NULL DEFAULT 0 COMMENT '系统管理员用户ID',
  `system_user_account` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '系统管理员用户账号',
  `system_menu_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '系统菜单名称',
  `system_menu_after_route` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统菜单路由地址',
  `request_method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `request_ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求IP',
  `request_country` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求国家',
  `request_province` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求省份',
  `request_city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求城市',
  `request_region` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求区县',
  `request_isp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求用户的网络运营商',
  `request_os` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求客户端设备类型',
  `request_browser` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求客户端浏览器类型',
  `request_user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求浏览器UA原始信息',
  `request_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
  `response_code` int(11) NULL DEFAULT NULL COMMENT '返回码',
  `response_message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回描述',
  `response_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回参数',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `system_user_id`(`system_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_system_user_log
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
