<p align="center">
<img src="public/static/logo-1.png" />
</p>

<p align="center">
<img src="https://svg.hamm.cn/badge.svg?key=ThinkPHP&value=>=6.0&color=528B22" />
<img src="https://svg.hamm.cn/badge.svg?key=PHP&value=>=8.0&color=6666ff" />
<img src="https://svg.hamm.cn/badge.svg?key=MySQL&value=>=5.7&color=6699FF" />
<img src="https://svg.hamm.cn/badge.svg?key=License&value=MIT&color=66CCFF" />
<img src="https://svg.hamm.cn/badge.svg?key=Coverage&value=55%&color=FF6600" />
<img src="https://svg.hamm.cn/badge.svg?key=Version&value=Beta-1.0.1&color=FF6600" />
</p>

------------

> 目前该项目还在持续开发中,暂不适用于直接二次开发商业系统,欢迎您持续关注我们

## 👨‍💻‍项目介绍

---

<img src="public/static/logo.svg" width="18px">OpenAdmin基于 ThinkPHP6 + Vue3.x + TypeScript + Arco Design Pro
等最新的技术栈开源的企业级后台管理系统,我们的初衷是不断的更新迭代使用最新的技术栈创建完善的/简单的/快速的后台管理系统,支持CRUD代码生成,ApiDoc自动生成接口文档,
使用最为宽松的MIT开源协议,无需任何授权即可免费商用,希望可以减少大家重复代码的时间,多去陪陪家人,陪陪孩子!

### 🚀项目架构

---

+ 后端使用ThinkPHP6.0框架进行开发,适合新手,国内社区环境较好,文档齐全
+ 基于PHP8.0 + 完全遵循PSR代码规范,性能较前置版本有很大提升
+ 前后端完全分离,后端遵循RESTful API风格开发
+ 前端基于Vue3.x 完全使用TypeScript
+ 前端框架使用Arco Design Pro 2.x Vue版本简洁大气美观

> 未来根据时间计划,还会增加[Hyperf框架版本][Laravel框架版本][JAVA语言版本][GOLang语言版本]等等!

### 💥软件功能

---

+ 已完成功能
    - [x] 账户管理 后台系统操作者,支持单账户多角色划分.
    - [x] 菜单管理 配置系统菜单,操作权限,按钮,栏目等权限标识等.
    - [x] 角色管理 角色菜单权限分配、设置角色按菜单范围权限划分.
    - [x] 操作日志 用户后台操作日志，全局异常、SQL注入等记录.
    - [x] 系统配置 系统的一些常用设置管理.
    - [x] 代码生成 使用Apidoc自动生成后端代码\路由\以及动态获取文档路由地址[2022年8月24日-v1.0.1].

+ 待完成功能
    - [ ] 附件管理 管理当前系统上传的文件及图片等信息.
    - [ ] 定时任务 在线（添加、修改、删除)任务调度包含执行结果日志.
    - [ ] 队列管理 消息队列管理功能、消息管理、消息发送.
    - [ ] OSS存储 支持各大云OSS运营商,支持后台上传,支持前台上传,大文件/分片等.
    - [ ] 推送管理 支持各大短信运营商,支持微信/钉钉/飞书等通知.
    - [ ] 微信管理 公众号/小程序/微信支付/等功能的集成.
    - [ ] 插件市场 根据使用情况酌情考虑是否支持插件开发,以及插件市场.

### 🌴环境要求

---

+ 推荐使用宝塔面板管理服务器环境
+ PHP >= 8.0 并开启一下扩展
    - [x] sodium
    - [x] simplexml
    - [x] libxml
    - [x] openssl
    - [x] redis
    - [x] fileinfo
    - [ ] opcache 推荐开启可大大提升PHP脚本运行效率
+ 开发环境下关闭禁用PHP函数exec、putenv、proc_open、proc_get_status、pcntl_signal[如果存在]
+ Mysql >=5.7
+ Redis >=6.0
+ Apache 或者 Nginx

### 📝安装教程

---

1. 安装后端

``` 
克隆项目
 * git clone https://gitee.com/open_admin/OpenAdmin.git
进入项目目录
 * cd OpenAdmin
确保您的环境已经安装了Composer包管理器,本项目Composer配置文件已添加腾讯源,无需另行配置[ps:小白可自行百度]
安装依赖包
 * composer install
导入数据库
数据库文件：public/openadmin.sql
修改配置
重命名[.env.example]文件名修改为[.env]文件,修改文件内对应配置
```

2. 安装前端

+ 本项目为前后端分离开发,请移步前端仓库
    + [Gitee OpenAdminWeb](https://gitee.com/open_admin/OpenAdminWeb)

### 🎉文档演示

---

> 正在完善中

### 🌈截图预览

---

<table>
    <tr>
        <td><img src="public/static/demo1.png"></td>
        <td><img src="public/static/demo2.png"></td>
    </tr>
    <tr>
        <td><img src="public/static/demo3.png"></td>
        <td><img src="public/static/demo4.png"></td>
    </tr>
</table>

### 💕特别鸣谢

---

> 感谢以下开源团队或作者,排名不分先后

+ [ThinkPHP](http://www.thinkphp.cn/)
+ [ThinkPHP ApiDoc](https://gitee.com/hg-code/thinkphp-apidoc/)
+ [w7corp/easywechat](https://easywechat.com/)
+ [firebase/php-jwt](https://github.com/firebase/php-jwt/)
+ [chinayin/ip2region](https://github.com/chinayin/ip2region-sdk-php/)
+ [jenssegers/agent](https://github.com/jenssegers/agent/)
+ [guzzlehttp/guzzle](https://github.com/guzzle/guzzle/)

### 👍问题反馈

---

+ 有任何疑问或者建议,可以提交[Issue](https://gitee.com/open_admin/OpenAdmin/issues/),或交流群内反馈
+ 交流群等待创建

### 🔐版权信息

---

> [OpenAdmin] 遵循MIT开源协议发布,并提供免费使用!

> 使用本框架不得用于开发违反国家有关政策的相关软件和应用,否则要付法律责任!

### 💖支持项目

---

> 打赏就duck不必啦～ 就点点🌟 Star 🌟 关注更新，支持下作者就可以了