<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About
// +----------------------------------------------------------------------
// | ApiDoc自动生成 {$form.controller_title}验证器
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace {$validate.namespace};

use think\Validate;

class {$validate.class_name} extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id' => ['require', 'integer', 'number'],
    {foreach $tables[0].datas as $k=>$item}
        {if '{$item.check}' && '{$item.field}' != 'id'}'{$item.field}' => [{foreach $item.check as $j=>$checkItem}'{$checkItem.value}'{if {$j}<{$count(item.check)}-1},{/if}{/foreach}], {/if}
        {if !'{$item.check}' && '{$item.not_null}' && '{$item.field}' != 'id'}'{$item.field}' => [require], {/if}
    {/foreach}
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
    {foreach $tables[0].datas as $k=>$item}
    {if '{$item.check}'}
        {foreach $item.check as $j=>$checkItem}'{$item.field}.{$checkItem.value}' => '{$checkItem.message}',
        {/foreach}
    {/if}
    {/foreach}
    ];

    // 验证场景
    protected $scene = [
        'add'   => [{foreach $tables[0].datas as $k=>$item}{if '{$item.add}' && ('{$item.not_null}' || '{$item.check}')}'{$item.field}', {/if}{/foreach}],
        'edit'  => [{foreach $tables[0].datas as $k=>$item}{if '{$item.edit}' && ('{$item.not_null}' || '{$item.check}')}'{$item.field}', {/if}{/foreach}],
        'read'  => ['id'],
    ];

    //删除场景剔除规则
    public function sceneDel()
    {
        return $this->only(['id'])->remove('id', ['integer', 'number']);
    }
}
