<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About
// +----------------------------------------------------------------------
// | ApiDoc自动生成 {$form.controller_title} 控制器
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace {$controller.namespace};

use think\Response;
use app\common\enum\SystemEnum;
use hg\apidoc\annotation as Apidoc;
use think\db\exception\DbException;
use app\common\controller\AdminController;
use {$tables[0].namespace}\{$tables[0].model_name};
use {$validate.namespace}\{$validate.class_name};
use {$cache.namespace}\{$cache.class_name};

/**
 * @Apidoc\Title("{$form.controller_title}")
 * @Apidoc\Group("{$form.group}")
 * @Apidoc\Sort("999")
 */
class {$controller.class_name} extends AdminController
{
    /**
     * 初始化方法
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->model = new {$tables[0].model_name}();
    }

    /**
     * @Apidoc\Title("列表")
     * @Apidoc\Param(ref="indexParam")
     * @Apidoc\Returned(ref="indexReturn")
     * @Apidoc\Returned("data", type="array", desc="内容列表",
     *     @Apidoc\Returned(ref="{$tables[0].model_path}\{$tables[0].model_name}")
     * )
     * @return Response
     * @throws DbException
     */
    public function index(): Response
    {
        [$page, $limit, $where, $order] = $this->buildSelect();
        $data = $this->model->where($where)->order($order)->paginate(['list_rows' => $limit, 'page' => $page]);
        return $this->result($data->isEmpty() ? [] : $data->toArray());
    }

    /**
     * @Apidoc\Title("添加")
     * @Apidoc\Method("POST")
     * @Apidoc\Param(ref="{$tables[0].model_path}\{$tables[0].model_name}",withoutField="id,create_time,update_time,delete_time")
     * @Apidoc\Returned(ref="{$tables[0].model_path}\{$tables[0].model_name}")
     */
    public function add(): Response
    {
        $this->validate($this->param, {$validate.class_name}::class, 'add');
        $data = $this->model;
        $data->save($this->param);
        return $this->result($data->toArray());
    }

    /**
     * @Apidoc\Title("详情")
     * @Apidoc\Param(ref="{$tables[0].model_path}\{$tables[0].model_name}",field="id")
     * @Apidoc\Returned(ref="{$tables[0].model_path}\{$tables[0].model_name}")
     */
    public function read(): Response
    {
        $this->validate($this->param, {$validate.class_name}::class, 'read');
        $data = {$cache.class_name}::get($this->param['id']);
        if (empty($data)) {
            $data = $this->model->findOrEmpty($this->param['id']);
            if ($data->isEmpty()) {
                return $this->error(SystemEnum::DATA_NOT_EXIST);
            }
            $data = $data->toArray();
            {$cache.class_name}::set($this->param['id'], $data);
        }
        return $this->result($data);
    }

    /**
     * @Apidoc\Title("修改")
     * @Apidoc\Method("POST")
     * @Apidoc\Param(ref="{$tables[0].model_path}\{$tables[0].model_name}",withoutField="create_time,update_time,delete_time")
     * @Apidoc\Returned(ref="{$tables[0].model_path}\{$tables[0].model_name}")
     * @return Response
     */
    public function edit(): Response
    {
        $this->validate($this->param, {$validate.class_name}::class, 'edit');
        $data = $this->model->findOrEmpty($this->param['id']);
        if ($data->isEmpty()) {
            return $this->error(SystemEnum::EDIT_DATA_ERROR);
        } else {
            $data->save($this->param);
        }
        return $this->result($data->toArray());
    }

    /**
     * @Apidoc\Title("删除")
     * @Apidoc\Method("POST")
     * @Apidoc\Param("id",type="array|int",desc="主键ID")
     * @return Response
     */
    public function del(): Response
    {
        $this->validate($this->param, {$validate.class_name}::class, 'del');
        $data = $this->model->destroy($this->param['id']);
        if ($data) {
            return $this->result([]);
        }
        return $this->error(SystemEnum::DELETE_DATA_ERROR);
    }
}
