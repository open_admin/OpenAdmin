<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About
// +----------------------------------------------------------------------
// | ApiDoc自动生成 {$form.controller_title}缓存
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace {$cache.namespace};

use app\common\cache\BaseCache;

class {$cache.class_name} extends BaseCache
{
    /**
     * 缓存名称前缀-不设置则使用默认前缀
     * @var string
     */
    protected static string $cache_name = '{$tables[0].table_name}';
}
