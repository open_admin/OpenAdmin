<?php
// +----------------------------------------------------------------------
// | OpenAdmin [ 基于ThinkPHP6和Vue3后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022 OpenAdmin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed (https://gitee.com/open_admin/OpenAdmin/blob/master/LICENSE)
// +----------------------------------------------------------------------
// | 作者: About
// +----------------------------------------------------------------------
// | ApiDoc自动生成 {$form.controller_title}模型
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace {$tables[0].namespace};

use think\Model;
use think\model\concern\SoftDelete;
use {$cache.namespace}\{$cache.class_name};

class {$tables[0].model_name} extends Model
{
    //引入软删除trait
    use SoftDelete;

    // 表名
    protected $name = '{$tables[0].table_name}';

    /**
     * 更新数据后的操作
     * @param $model //当前模型的对象实例
     * @return void
     */
    public static function onAfterUpdate($model): void
    {
        {$cache.class_name}::delete($model->id);
    }

    /**
     * 删除数据后的操作
     * @param $model //当前模型的对象实例
     * @return void
     */
    public static function onAfterDelete($model): void
    {
        {$cache.class_name}::delete($model->id);
    }
}
